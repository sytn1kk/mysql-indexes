# MySQL Indexes
## Node.js, MySQL, Artillery

## Run

- ```git clone https://gitlab.com/sytn1kk/mysql-indexes.git```
- ```cd mysql-indexes```
- Chane MySQL configurations
- Run load testing ```docker run --network=mysql-indexes_default --rm -it \
      -v "$(pwd)/artillery/":/scripts \
      artilleryio/artillery:latest \
      run /scripts/load-testing.yml```

## innodb_flush_log_at_trx_commit testing results

### SET GLOBAL innodb_flush_log_at_trx_commit = 1;

```
All virtual users finished
Summary report @ 17:38:08(+0200) 2021-11-21
  Scenarios launched:  6750
  Scenarios completed: 6750
  Requests completed:  6750
  Mean response/sec: 148.42
  Response time (msec):
    min: 7
    max: 702
    median: 236.5
    p95: 596
    p99: 633
  Scenario counts:
    0: 6750 (100%)
  Codes:
    200: 6750
```

### SET GLOBAL innodb_flush_log_at_trx_commit = 2;

```
All virtual users finished
Summary report @ 17:35:30(+0200) 2021-11-21
  Scenarios launched:  6750
  Scenarios completed: 6750
  Requests completed:  6750
  Mean response/sec: 146.74
  Response time (msec):
    min: 6
    max: 1023
    median: 36
    p95: 893
    p99: 960
  Scenario counts:
    0: 6750 (100%)
  Codes:
    200: 6750
```

### SET GLOBAL innodb_flush_log_at_trx_commit = 0;

```
All virtual users finished
Summary report @ 17:39:44(+0200) 2021-11-21
  Scenarios launched:  6750
  Scenarios completed: 6750
  Requests completed:  6750
  Mean response/sec: 148.29
  Response time (msec):
    min: 7
    max: 148
    median: 19
    p95: 48
    p99: 73
  Scenario counts:
    0: 6750 (100%)
  Codes:
    200: 6750
```

## Indexes

select without index - 21s, rows - 38970628
select with hash index - 128ms, rows - 1
select with btree index - 100ms, rows - 1

## Screenshots
![Explain without index](https://i2.paste.pics/ff1c23e985e518d37a892c833e526d63.png "Explain without index")
![Explain with index](https://i2.paste.pics/a3aff50c3347878ff91a89d657d56118.png "Explain with index")

