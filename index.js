const express = require('express')
const { PrismaClient } = require('@prisma/client')

const { uniqueNamesGenerator, names } = require('unique-names-generator')

const app = express()
const prisma = new PrismaClient()

const START_DATE = new Date(2012, 0, 1)
const END_DATE = new Date()

app.post('/', async (_req, res) => {
    await prisma.user.create({
        data: {
            name: uniqueNamesGenerator({
                dictionaries: [names],
            }),
            date_of_birth: new Date(START_DATE.getTime() + Math.random() * (END_DATE - START_DATE.getTime()))
        }
    }).then(console.log)
    res.send()
})

async function main() {
    app.listen(3000);
}

main()
  .catch((e) => {
    throw e
  })
  .finally(async () => {
    await prisma.$disconnect()
  })