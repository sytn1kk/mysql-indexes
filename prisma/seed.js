const { PrismaClient } = require('@prisma/client')
const { uniqueNamesGenerator, names } = require('unique-names-generator')

const prisma = new PrismaClient()

const nameConfig = {
    dictionaries: [names],
}

const START_DATE = new Date(2012, 0, 1)
const END_DATE = new Date()
const BULK_SIZE = 100000
const BULK_LIMIT = 400

const randomDate = () => {
    return new Date(START_DATE.getTime() + Math.random() * (END_DATE - START_DATE.getTime()));
}

const createBulk = () => {
    const bulk = [];
    for (let i = 0; i < BULK_SIZE; i++) {
        bulk.push({
            name: uniqueNamesGenerator(nameConfig),
            date_of_birth: randomDate(),
        })
    }
    return bulk;
}

async function main() {
    for (let i = 0; i < BULK_LIMIT; i++) {
        const insert = await prisma.user.createMany({
            data: createBulk(),
        })
        console.log(insert)
    }
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })