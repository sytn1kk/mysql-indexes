FROM node:lts

WORKDIR /app

COPY package.json /app/

RUN npm install

COPY . .

RUN npx prisma db seed

CMD [ "npm", "run", "start" ]